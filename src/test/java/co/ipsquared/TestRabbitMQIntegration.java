package co.ipsquared;

import co.ipsquared.rabbitmq.generic.TestRabbitMQGenericRoutingConfiguration;
import co.ipsquared.rabbitmq.generic.TestRabbitMQGenericRoutingListener;
import co.ipsquared.rabbitmq.json.TestMessage;
import co.ipsquared.rabbitmq.json.TestRabbitMQJasonConfiguration;
import co.ipsquared.rabbitmq.json.TestRabbitMQJsonMessageListener;
import co.ipsquared.rabbitmq.text.TestRabbitMQTextConfiguration;
import co.ipsquared.rabbitmq.text.TestRabbitMQTextMessageListener;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


/**
 * Created by FNB
 *
 * @author nik - F3467902
 *         Email: nvandertoorn@gmail.com
 *         Date: 2016/03/07
 *         <p/>
 *         The purpose of this class is to unit test sending and receiving messages over RabbitMQ
 */
public class TestRabbitMQIntegration {

    private static final Logger LOG = Logger.getLogger(TestRabbitMQIntegration.class);

    @Before
    public void setUp() throws Exception {
        System.setProperty("rabbitmq-host", "localhost");
        System.setProperty("rabbitmq-virtual-host", "/test-rabbit");
        System.setProperty("rabbitmq-user", "test-rabbit");
        System.setProperty("rabbitmq-password", "test-rabbit");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testSendAndReceiveTestJsonMessage() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(TestRabbitMQJasonConfiguration.class);
        RabbitTemplate rabbitTemplate = ctx.getBean(RabbitTemplate.class);
        rabbitTemplate.convertAndSend(new TestMessage(666, "RabbitMQ Test JSON"));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            //Do nothing
        }
        TestRabbitMQJsonMessageListener testRabbitMQMessageListener = (TestRabbitMQJsonMessageListener)ctx.getBean("messageListener");
        Assert.assertEquals(666, testRabbitMQMessageListener.getMessage().getId());
        LOG.info("All passed!");
    }

    @Test
    public void testSendAndReceiveTestTextMessage() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(TestRabbitMQTextConfiguration.class);
        RabbitTemplate rabbitTemplate = ctx.getBean(RabbitTemplate.class);
        rabbitTemplate.convertAndSend("RabbitMQ Test Text");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            //Do nothing
        }
        TestRabbitMQTextMessageListener testRabbitMQMessageListener = (TestRabbitMQTextMessageListener)ctx.getBean("messageListener");
        Assert.assertEquals("RabbitMQ Test Text", testRabbitMQMessageListener.getMessage());
        LOG.info("All passed!");
    }

    @Test
    public void testSendAndReceiveGenericRoutingMessage() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(TestRabbitMQGenericRoutingConfiguration.class);
        RabbitTemplate rabbitTemplate = ctx.getBean(RabbitTemplate.class);
        rabbitTemplate.convertAndSend("test.generic.key.evolution", "RabbitMQ Test Generic Routing");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            //Do nothing
        }
        TestRabbitMQGenericRoutingListener rabbitMQGenericRoutingListener = (TestRabbitMQGenericRoutingListener)ctx.getBean("messageListener");
        Assert.assertEquals("RabbitMQ Test Generic Routing", rabbitMQGenericRoutingListener.getMessage());
        LOG.info("All passed!");
    }

}
