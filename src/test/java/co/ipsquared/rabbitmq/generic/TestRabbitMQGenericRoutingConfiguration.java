package co.ipsquared.rabbitmq.generic;

import co.ipsquared.rabbitmq.RabbitMQConfiguration;
import org.springframework.amqp.core.MessageListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by FNB
 *
 * @author nik - F3467902
 *         Email: nvandertoorn@gmail.com
 *         Date: 2016/03/09
 *         <p/>
 *         The purpose of this class is to provide the configuration details for testing sending and receiving messages
 *         over RabbitMQ
 */
@Configuration
@ComponentScan("co.ipsquared.rabbitmq.generic")
public class TestRabbitMQGenericRoutingConfiguration extends RabbitMQConfiguration {

    public String queueName() {
        return "test.generic.queue";
    }

    public String exchangeName() {
        return "test.generic.exchange";
    }

    public String routingKey() {
        return "test.generic.key.*";
    }

    @Override
    @Bean
    public MessageListener messageListener() {
        return new TestRabbitMQGenericRoutingListener();
    }

}
