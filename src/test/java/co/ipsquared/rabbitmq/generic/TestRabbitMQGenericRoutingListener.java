package co.ipsquared.rabbitmq.generic;

import co.ipsquared.rabbitmq.AbstractMessageListener;
import org.apache.log4j.Logger;

/**
 * Created by FNB
 *
 * @author nik - F3467902
 *         Email: nvandertoorn@gmail.com
 *         Date: 2016/03/09
 *         <p/>
 *         The purpose of this class is to provide a listener for testing the receiving of messages over RabbitMQ
 *
 */
public class TestRabbitMQGenericRoutingListener extends AbstractMessageListener<String> {

    private static final Logger LOG = Logger.getLogger(TestRabbitMQGenericRoutingListener.class);

    @Override
    protected void handleMessage(String message) {
        LOG.info("handleMessage: " + message);
    }

}
