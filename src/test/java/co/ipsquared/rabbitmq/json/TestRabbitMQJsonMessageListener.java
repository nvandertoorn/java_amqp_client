package co.ipsquared.rabbitmq.json;

import co.ipsquared.rabbitmq.AbstractMessageListener;
import org.apache.log4j.Logger;

/**
 * Created by FNB
 *
 * @author nik - F3467902
 *         Email: nvandertoorn@gmail.com
 *         Date: 2016/03/09
 *         <p/>
 *         The purpose of this class is to provide a listener for testing the receiving of messages over RabbitMQ
 *
 */
public class TestRabbitMQJsonMessageListener extends AbstractMessageListener<TestMessage> {

    private static final Logger LOG = Logger.getLogger(TestRabbitMQJsonMessageListener.class);

    @Override
    protected void handleMessage(TestMessage message) {
        LOG.info("handleMessage: " + message);
    }

}
