package co.ipsquared.rabbitmq.json;

/**
 * Created by FNB
 *
 * @author nik - F3467902
 *         Email: nvandertoorn@gmail.com
 *         Date: 2016/03/09
 *         <p/>
 *         The purpose of this class is to provide a POJO for testing to and from JSON conversion over RabbitMQ
 */
public class TestMessage {

    private int id;
    private String name;

    public TestMessage() {
    }

    public TestMessage(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "TestMessage{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
