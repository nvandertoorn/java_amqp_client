package co.ipsquared.rabbitmq.text;

import co.ipsquared.rabbitmq.RabbitMQConfiguration;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by FNB
 *
 * @author nik - F3467902
 *         Email: nvandertoorn@gmail.com
 *         Date: 2016/03/09
 *         <p/>
 *         The purpose of this class is to provide the configuration details for testing sending and receiving messages
 *         over RabbitMQ
 */
@Configuration
@ComponentScan("co.ipsquared.rabbitmq.text")
public class TestRabbitMQTextConfiguration extends RabbitMQConfiguration {

    public String queueName() {
        return "test.text.queue";
    }

    public String exchangeName() {
        return "test.text.exchange";
    }

    public String routingKey() {
        return "test.text.key";
    }

    @Override
    @Bean
    public MessageListener messageListener() {
        return new TestRabbitMQTextMessageListener();
    }

}
