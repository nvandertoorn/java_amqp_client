package co.ipsquared.rabbitmq;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.support.converter.JsonMessageConverter;


/**
 * Created by FNB
 *
 * @author nik - F3467902
 *         Email: nvandertoorn@gmail.com
 *         Date: 2016/03/09
 *         <p/>
 *         The purpose of this class is to TODO: put own class description here
 */
public abstract class AbstractMessageListener<T> implements MessageListener {

    private static final Logger LOG = Logger.getLogger(AbstractMessageListener.class);
    private JsonMessageConverter jsonMessageConverter = new JsonMessageConverter();

    private T message;

    public void onMessage(Message rabbitMqMessage) {
        LOG.info("onMessage: " + rabbitMqMessage);
        message = (T)jsonMessageConverter.fromMessage(rabbitMqMessage);
        handleMessage(message);
    }

    protected abstract void handleMessage(T message);

    public T getMessage() {
        return message;
    }
}
